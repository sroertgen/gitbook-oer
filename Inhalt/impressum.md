# Impressum

**Angaben gemäß § 5 Telemediengesetz**

### Herausgeber

### Autorin


### Kontakt


### Videoproduktion

### Technischer Support


### Logo 


### Zuständige Aufsichtsbehörde

# Rechtliche Hinweise

## Inhalte des Online-Angebotes

Der Anbieter stellt den Nutzern im Rahmen seiner technischen und wirtschaftlichen Möglichkeiten diesen Dienst unentgeltlich zur Verfügung. Der Anbieter ist bemüht seinen Dienst verfügbar zu halten. Der Anbieter übernimmt keine darüberhinausgehenden Leistungspflichten, insbesondere besteht kein Anspruch des Nutzers auf eine ständige Verfügbarkeit des Dienstes. Der Anbieter übernimmt keine Gewähr für die Richtigkeit, Vollständigkeit, Aktualität und Brauchbarkeit der bereitgestellten Inhalte.
Der Anbieter ist berechtigt, Änderungen an seinem Dienst vorzunehmen und seinen Dienst jederzeit ohne Ankündigung zu beenden.

## Links und Verweise

Das Dienstangebot enthält Verknüpfungen zu Webseiten Dritter (externe Links). Diese Webseiten unterliegen der Haftung der jeweiligen Betreiber. Der Anbieter hat keinerlei Einfluss auf die aktuelle und zukünftige Gestaltung und auf die Inhalte der verknüpften Seiten. Das Setzen von Links bedeutet nicht, dass sich der Anbieter diese Inhalte zu eigen macht. Eine ständige Kontrolle der externen Inhalte ist für den Anbieter ohne konkrete Hinweise auf Rechtsverstöße nicht zumutbar. Bei Kenntnisnahme von Rechtsverstößen werden jedoch derartige externe Inhalte unverzüglich gelöscht.

## Urheber- und Kennzeichenrecht

Das Urheberecht für Texte und Bilder liegt, soweit nicht anders gekennzeichnet, beim Anbieter.
Auf den Webseiten zur Verfügung gestellte Texte, Textteile, Grafiken, Tabellen oder Bildmaterialien dürfen ohne ausdrückliche, vorherige Zustimmung des Anbieters nicht vervielfältig, verbreitet oder ausgestellt werden.
