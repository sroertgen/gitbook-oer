# Text 1

Dies ist der erste Text von Kapitel 1.

Und hier ist ein Video eingebettet:

<iframe width="560" height="315" src="https://www.youtube.com/embed/1WnZD7E8FKY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Das ist der Code, um ein Video einzubetten (bspw. über YouTube -> Teilen -> Einbetten):

```html
<iframe width="560" height="315" src="https://www.youtube.com/embed/1WnZD7E8FKY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
```

Und hier ist ein Bild von einer Katze:

![Alternativ-Text: Bild von einer süßen Katze](../Bilder/cat_small.jpeg "Hier kann noch ein optionaler Titel eingefügt werden: Bild von einer süßen Katze veröffentlicht unter CC0 von Kelvin Valerio. Abrufbar unter https://www.pexels.com/photo/adorable-animal-blur-cat-617278/" )

Das ist der Markdown-Code, um das Bild einzubetten:

```markdown
![Alternativ-Text: Bild von einer süßen Katze](../Bilder/cat_small.jpeg "Hier kann noch ein optionaler Titel eingefügt werden: Bild von einer süßen Katze veröffentlicht unter CC0 von Kelvin Valerio. Abrufbar unter https://www.pexels.com/photo/adorable-animal-blur-cat-617278/" )
```
