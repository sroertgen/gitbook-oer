# Gitbook für OER

Hier muss eine Projektbeschreibung eingefügt werden.

## How to fork

Nach dem Forken müssen in der Datei `book.json` zwei Pfade angepasst werden.
Ihr müsst dort jeweils die Url eintragen, die ihr in GitLab unter `"Settings" > "Pages"` findet (links in der Seitenleiste).
Normalerweise sieht die Url so ähnlich aus: <https://sroertgen.gitlab.io/gitbook-oer> (nachdem ihr ein Projekt neu angelegt habt, kann es ein paar Minuten dauern, bis die URL dort erscheint).

* in Zeile 2: `"base_path": "<eure-url-eintragen>",`
  * dort bitte die eigene  GitLab-Pages-Url eintragen
* in Zeile 26: `"base": "https://<eure-url-eintragen>/book_pdf/book.pdf?/",`
  * dort bitte ebenfalls die eigene GitLab-Pages-Url eintragen

Anschließend wird nach jeder Änderung ein unter der o.g. URL ein neues GitBook erzeugt.


## How to use

### Struktur des GitBooks

* Autor:innen können einzelne Seiten als Markdown-Datei verfassen und diese in dem Ordner **Inhalt** ablegen. Der Name des Ordner spielt dabei keine Rolle, jedoch dient es der Übersichtlichkeit die Dateien dort abzulegen. In dem Ordner können auch beliebig viele Unterordner angelegt werden. Einen Markdown-Guide findest Du beispielsweise [hier](https://gitlab.com/francoisjacquet/rosariosis/-/wikis/Markdown-Cheatsheet)
* In der Datei `SUMMARY.md` findet die **Organisation des GitBooks** statt. Du definierst damit, in welcher Reihenfolge die Dateien in deinem GitBook angezeigt werden. So könnte deine `SUMMARY.md` aussehen:

```markdown
# Summary

* [Willkommen](README.md)

## Kapitel 1

* [Text 1](Inhalt/Kapitel_1/Text_1.md)
* [Text 2](Inhalt/Kapitel_1/Text_2.md)
  * [Unterkapitel 1](Inhalt/Kapitel_1/Unterkapitel_1.md)

## Projektinformationen

* [Impressum](Inhalt/impressum.md)
* [Datenschutzerklärung](Inhalt/datenschutzerklärung.md)
```

**HINWEIS:** Die erste Seite eines GitBooks muss immer die `README.md` sein!

  * Das Dokument muss mit `#Summary` beginnen
  * Unterüberschriften werden mit zwei Hashtags (##) gekennzeichnet
  * Die einzelnen Dokumente werden wie Markdown-Links eingefügt: `[Titel des Dokuments](Inhalt/Link-zum-Dokument.md)`
  * Weitere Unterkapitel entstehen durch weitere Einrückung (zwei Leerzeichen):
  
```markdown
* [Erstes Kapitel](Inhalt/Kapitel_1.md)
    *[Unterkapitel](Inhalt/Unterkapitel.md)
```

### Bilder einfügen

* **Bilder** und andere Medien sollten in einem Ordner unter `Inhalt/Bilder` abgelegt werden. Von dort aus können sie an beliebiger Stelle in Dokumenten mit `![Bildbeschreibender Text](../Bilder/Euer_Bild.jpg)` eingebunden werden.

### Sonstiges

* in die Datei `book.json` sollten in den Zeilen 43-45 euer Name, Titel und Beschreibung des Projekts eingefügt werden.
* In der Datei `layouts/website/summary.html` kann in Zeile 71 der Link angepasst werden, um auf Mitarbeitsmöglichkeiten in eurem Projekt zu verweisen. Bspw: `<a target="_blank" href="https://gitlab.com/sroertgen/la-digital-git-book">Mitarbeiten am Projekt</a>` 

* Die Datei `Inhalt/datenschutzerklärung.md` und `Inhalt/impressum.md` sollten je nach euren Erfordernissen angepasst werden.

* Um ein PDF zu generieren, muss eine Datei namens `cover.jpg` vorhanden sein. Diese wird später als Cover für das generierte PDF dienen. Ihr könnt dafür eine beliebig A4 große .jpg-Datei verwenden.

## Hypothes.is

Um Annotationen auf der Webseite zu ermöglichen, wurde das Tool [hyptothes.is](https://web.hypothes.is/) eingebunden. Damit erscheint auf der rechten Seite der Webpage eine Seitenleiste. Dort kann sich eingeloggt werden, um anschließend private oder öffentlich sichbare Anmerkungen oder Hervorhebungen auf der Webseite zu hinterlegen.

Um das Tool zu deaktivieren, müssen in der Datei `_layouts/website/page.html` die Zeile 4: `<script src="https://hypothes.is/embed.js" async></script>` und die Zeile 57: `location.reload();` entfernt werden.

------------------------------------------------------------------------


<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br>

Dieses Projekt ist eine Fork des [OERientation](https://oerientation.rz.tuhh.de/)-Projektes des
[Instituts für Technische Bildung und Hochschuldidaktik (ITBH)](http://itbh-hh.de/de/) an der
[TU Hamburg-Harburg](https://www.tuhh.de/) und wurde dort von Sabrina Maaß entwickelt. 
Das Projekt ist lizenziert unter einer [Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International Lizenz](https://creativecommons.org/licenses/by-sa/4.0/legalcode.de).

